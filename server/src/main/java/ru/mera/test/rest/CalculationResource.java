/**
 * Info about this package doing something for package-info.java file.
 */
package ru.mera.test.rest;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;
import java.util.OptionalDouble;

/**
*  Documentation.
*/
@Path("/calculation")
@XmlAccessorType(XmlAccessType.NONE)
public class CalculationResource {

  /**
   * value.
   */
  private static final int EXITCODE = 501;

  /**
   * blah-blah-blah.
   *
   * @return value
   */
  @GET
  public final String calculationInfo() {
    return "Calculation resource v1";
  }

  /**
   * blah-blah-blah.
   *
   * @param summands param
   * @return value
   */
  @Path("/addition")
  @Produces("application/json")
  @GET
  public final String addition(
    @QueryParam("operand") final List<Double> summands) {
    Double result = summands.stream().mapToDouble(Double::doubleValue).sum();
    return result.toString();
  }

  /**
   * blah-blah-blah.
   *
   * @param operands param
   * @return value
   */
  @Path("/subtraction")
  @Produces("application/json")
  @GET
  public final String subtraction(
    @QueryParam("operand") final List<Double> operands) {
    OptionalDouble result = operands.stream().mapToDouble(Double::doubleValue)
                             .reduce((left, right) -> left - right);
    double resultDouble = result.orElseGet(() -> {
      throw new WebApplicationException(Response.status(EXITCODE)
        .entity("result is null").build());
    });
    return Double.toString(resultDouble);
  }

  /**
   * blah-blah-blah.
   *
   * @param operands param
   * @return value
   */
  @Path("/multiplication")
  @Produces("application/json")
  @GET
  public final String multiplication(
    @QueryParam("operand") final List<Double> operands) {
    OptionalDouble result = operands.stream().mapToDouble(Double::doubleValue)
                              .reduce((left, right) -> left * right);
    double resultDouble =  result.orElseGet(() -> {
      throw new WebApplicationException(Response.status(EXITCODE)
        .entity("result is null").build());
    });
    return Double.toString(resultDouble);
  }

  /**
   * blah-blah-blah.
   *
   * @param operands param
   * @return value
   */
  @Path("/division")
  @Produces("application/json")
  @GET
  public final String division(
    @QueryParam("operand") final List<Double> operands) {
    OptionalDouble result = operands.stream().mapToDouble(Double::doubleValue)
                             .reduce((left, right) -> left / right);
    double resultDouble =  result.orElseGet(() -> {
      throw new WebApplicationException(Response.status(EXITCODE)
        .entity("result is null").build());
    });
    return Double.toString(resultDouble);
  }
}
