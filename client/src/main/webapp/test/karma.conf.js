// Karma configuration
// http://karma-runner.github.io/0.13/config/configuration-file.html

var sourcePreprocessors = ['webpack', 'sourcemap'];

function isDebug() {
    return process.argv.indexOf('--debug') >= 0;
}

if (isDebug()) {
    // Disable JS minification if Karma is run with debug option.
    sourcePreprocessors = ['webpack'];
}

module.exports = function (config) {
    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: 'test/'.replace(/[^/]+/g, '..'),

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],

        // testing framework to use (jasmine/mocha/qunit/...)
        frameworks: ['jasmine'],

        files: [
          // {pattern: 'test/**/*.spec.js'}
          {pattern: 'test/suite.js'}
          // each file acts as entry point for the webpack configuration
        ],

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: true,

        preprocessors: {
          // add webpack as preprocessor
            'test/suite.js': sourcePreprocessors,
        },


        reporters: ['dots', 'junit', 'coverage', 'progress'],

        junitReporter: {
            outputFile: '../dist/test-results/karma/TESTS-results.xml'
        },

        coverageReporter: {
            dir: 'dist/test-results/coverage',
            reporters: [
              {type: 'text-summary'},
              {type: 'html'}
            ]
        },

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        webpack: require('../webpack.config'),

        webpackMiddleware: {
          // webpack-dev-middleware configuration
          // i. e.
          stats: 'errors-only'
        },

        // to avoid DISCONNECTED messages when connecting to slow virtual machines
        browserDisconnectTimeout: 10000, // default 2000
        browserDisconnectTolerance: 1, // default 0
        browserNoActivityTimeout: 4 * 60 * 1000 //default 10000
    });
};
