// require('angular');
// require('angular-mocks/angular-mocks');
require('../../app/main');

describe('Service: mainService', function () {
  var service;

  beforeEach(angular.mock.module('main'));

  beforeEach(angular.mock.inject(['mainService', function(mainService) {
    service = mainService;
  }]));

  it('should be defined', function () {
    expect(service).toBeDefined();
  });
});
