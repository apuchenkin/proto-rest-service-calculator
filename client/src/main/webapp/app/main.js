var angular = require('angular');

require('angular-route');
require('angular-resource');

module.exports = angular.module('main', ['ngRoute', 'ngResource']).config([
    '$routeProvider',
    function($routeProvider) {

        $routeProvider.when('/', {
            controller: 'MainController',
            controllerAs: 'vm',
            template: require('./views/view.html'),
            caseInsensitiveMatch: true
        });
    }
]);

require('./app.controller');
